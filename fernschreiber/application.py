import fernschreiber.config_manager
import gi
import sys
gi.require_version('Gtk', '3.0')
gi.require_version('Handy', '0.0')
from gi.repository import GObject, GLib, Gio, Gtk, Handy
from fernschreiber.main_window import MainWindow


class Application(Gtk.Application):
    window = NotImplemented
    file_list = []
    development_mode = None
    application_id = "run.terminal.Fernschreiber"

    def __init__(self, *args, **kwargs):
        # Register LibHandy Responsive Column
        GObject.type_register(Handy.Column)

        super().__init__(
            *args, application_id=self.application_id, flags=Gio.ApplicationFlags.HANDLES_OPEN)
        self.window = None

    def do_startup(self):
        Gtk.Application.do_startup(self)
        GLib.set_application_name("Fernschreiber")
        GLib.set_prgname("Fernschreiber")

        if fernschreiber.config_manager.check_config_exists() is False:
            fernschreiber.config_manager.create_config_dirs()

    def do_activate(self):
        if not self.window:
            self.window = MainWindow(
                application=self, title="Fernschreiber",
                icon_name=self.application_id)

        self.window.present()


if __name__ == "__main__":
    app = Application()
    app.run(sys.argv)

