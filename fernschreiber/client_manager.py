from telethon import TelegramClient, events, sync
import asyncio
import threading
import queue
import fernschreiber.config_manager
import random


class ClientManager(threading.Thread):
    queue = NotImplemented
    timeout = NotImplemented
    event_loop = NotImplemented
    ret = NotImplemented
    exception = NotImplemented

    api_id = NotImplemented
    api_hash = NotImplemented
    session_id = NotImplemented

    phone_number = NotImplemented
    password = NotImplemented

    client = NotImplemented
    account = NotImplemented

    connected = False

    def __init__(self, loop_time=1.0/60):
        self.queue = queue.Queue()
        self.timeout = loop_time
        super(ClientManager, self).__init__()

    #
    # ClientThread Management
    #

    def run(self):
        while True:
            try:
                function, args, kwargs = self.queue.get(timeout=self.timeout)
                function(*args, **kwargs)
            except queue.Empty:
                self.idle()

    def run_on_thread(self, function, *args, **kwargs):
        self.queue.put((function, args, kwargs))

    def wait_for_throwable(self):
        return self.queue.get()

    def join(self):
        exc_ret_store = self.wait_for_throwable()
        if exc_ret_store is None:
            return
        else:
            raise exc_ret_store[1]

    def idle(self):
        pass

    def print_thread(self):
        print(threading.currentThread())

    #
    # Telegram Client Management
    #

    def initialize_manager(self):
        self.api_id = fernschreiber.config_manager.get_api_id()
        self.api_hash = fernschreiber.config_manager.get_api_hash()
        self.session_id = fernschreiber.config_manager.get_session_id()

        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)

        self.connect_client()

    #
    # Session Management
    #

    def connect_client(self):
        if not self.session_id:
            self.generate_session_id()

        self.client = TelegramClient(
            fernschreiber.config_manager.sessions_dir + self.session_id,
            int(self.api_id),
            self.api_hash,
            loop=self.loop
        )

        self.client.connect()

    def check_session_active(self):
        return self.client.is_user_authorized()

    def set_phone_number(self, phone_number):
        self.account = self.client.sign_in(phone_number)
        self.phone_number = phone_number

    def hand_in_code(self, code):
        self.account = self.client.sign_in(code=code)

    def hand_in_password(self, password, instance):
        self.account = self.client.sign_in(password=password)

    def send_code_request(self):
        self.client.send_code_request(self.phone_number)

    def log_in(self):
        fernschreiber.config_manager.set_session_id(self.session_id)
        self.connected = True

    def generate_session_id(self):
        self.session_id = "fernschreiber_" + str(random.getrandbits(128))

    def disconnect_client(self):
        self.client.disconnect()

