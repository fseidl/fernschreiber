from gi.repository import Gio, GLib
import os.path

setting = Gio.Settings.new("run.terminal.Fernschreiber")
config_dir = GLib.get_user_config_dir() + "/fernschreiber/"
sessions_dir = config_dir + "sessions/"
photos_dir = config_dir + "photos/"

api_id = "api-id"
api_hash = "api-hash"
session_id = "session-id"
window_size = "window-size"


def get_api_id():
    return setting.get_int(api_id)


def get_api_hash():
    return setting.get_string(api_hash)


def get_session_id():
    return setting.get_string(session_id)


def set_session_id(value):
    setting.set_string(session_id, value)


def get_window_size():
    return setting.get_value(window_size)


def set_window_size(list):
    g_variant = GLib.Variant('ai', list)
    setting.set_value(window_size, g_variant)


#
# Helpers
#

def check_config_exists():
    return os.path.exists(config_dir)


def create_config_dirs():
    os.makedirs(config_dir)
    os.makedirs(sessions_dir)
    os.makedirs(photos_dir)

