from gi.repository import Gtk
from fernschreiber.overview import Overview
import telethon.errors


class Login():
    builder = NotImplemented

    window = NotImplemented
    stack = NotImplemented

    def __init__(self, window):
        self.window = window
        self.builder = Gtk.Builder()
        self.builder.add_from_resource("/run/terminal/Fernschreiber/login.ui")

        self.set_headerbar()
        self.assemble_page()

    def set_headerbar(self):
        headerbar = self.builder.get_object("headerbar")
        self.window.set_titlebar(headerbar)

        self.builder.get_object("back_button").connect("clicked", self.on_back_button_clicked)

    def assemble_page(self):
        self.window.add(self.builder.get_object("login_page"))
        self.stack = self.builder.get_object("stack")

        self.builder.get_object("phone_next_button").connect(
            "clicked",
            self.on_phone_next_button_clicked
        )
        self.builder.get_object("code_next_button").connect(
            "clicked",
            self.on_code_next_button_clicked
        )
        self.builder.get_object("password_next_button").connect(
            "clicked",
            self.on_password_next_button_clicked
        )

    #
    # Events
    #

    def on_phone_next_button_clicked(self, button):
        self.window.client_manager.run_on_thread(self.window.client_manager.set_phone_number, self.builder.get_object("phone_entry").get_text())
        self.stack.set_visible_child(self.stack.get_child_by_name("code_page"))

    def on_code_next_button_clicked(self, button):
        try:
            self.window.client_manager.run_on_thread(self.window.client_manager.hand_in_code, self.builder.get_object("code_entry").get_text())
            self.window.client_manager.join()
        except telethon.errors.rpcerrorlist.SessionPasswordNeededError:
            self.stack.set_visible_child(self.stack.get_child_by_name(
                "password_page")
            )
        except(telethon.errors.rpcerrorlist.PhoneNumberUnoccupiedError, telethon.errors.rpcerrorlist.PhoneCodeInvalidError):
            self.builder.get_object("code_entry").get_style_context().add_class("error")

    def on_password_next_button_clicked(self, button):
        self.window.client_manager.run_on_thread(self.window.client_manager.hand_in_password, self.builder.get_object("password_entry").get_text())

        self.start_overview()

    def on_back_button_clicked(self, button):
        if self.stack.get_visible_child_name() == "code_page":
            self.stack.set_visible_child_name("phone_page")
        else:
            self.window.remove(self.builder.get_object("login_page"))
            self.window.create_headerbar()
            self.window.first_start_screen()
    #
    # Helpers
    #

    def start_overview(self):
        self.window.remove(self.builder.get_object("login_page"))
        Overview(self.window, self.window.client_manager)

