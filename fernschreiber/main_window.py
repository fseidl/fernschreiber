from gi.repository import Gtk
from gi.repository.GdkPixbuf import Pixbuf
from fernschreiber.client_manager import ClientManager
from fernschreiber.login import Login
from fernschreiber.overview import Overview
import fernschreiber.config_manager


class MainWindow(Gtk.ApplicationWindow):
    client_manager = NotImplemented
    first_start_grid = NotImplemented

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.assemble_window()

    def assemble_window(self):
        window_size = fernschreiber.config_manager.get_window_size()
        self.set_default_size(window_size[0], window_size[1])

        self.connect("delete-event", self.on_application_quit)
        self.create_headerbar()
        self.create_client_instance()
        self.first_start_screen()

    #
    # Headerbar
    #

    def create_headerbar(self):
        builder = Gtk.Builder()
        builder.add_from_resource("/run/terminal/Fernschreiber/main_window.ui")

        self.headerbar = builder.get_object("headerbar")
        self.set_titlebar(self.headerbar)

        builder.get_object("login_button").connect("clicked", self.on_login_button_clicked)

    def create_client_instance(self):
        self.client_manager = ClientManager()
        self.client_manager.start()
        self.client_manager.run_on_thread(self.client_manager.initialize_manager)

    def first_start_screen(self, welcome=True):
        if not fernschreiber.config_manager.get_session_id() or welcome is False:
            builder = Gtk.Builder()
            builder.add_from_resource(
                "/run/terminal/Fernschreiber/main_window.ui"
            )
            pix = Pixbuf.new_from_resource_at_scale(
                "/run/terminal/Fernschreiber/images/welcome.png",
                256,
                256,
                True
            )
            app_logo = builder.get_object("app_logo")
            app_logo.set_from_pixbuf(pix)
            self.first_start_grid = builder.get_object("first_start_grid")
            self.add(self.first_start_grid)
        else:
            self.create_login_page()

    def create_login_page(self):
        if self.first_start_grid is not NotImplemented:
            self.remove(self.first_start_grid)

        if self.client_manager.run_on_thread(self.client_manager.check_session_active) is True:
            self.start_overview()
        else:
            Login(self)

    def start_overview(self):
        Overview(self, self.client_manager)

    #
    # Events
    #

    def on_login_button_clicked(self, button):
        self.create_login_page()

    def on_application_quit(self, window, event):
        self.save_window_size()

        if self.client_manager is not NotImplemented:
            if self.client_manager.connected is True:
                self.client_manager.disconnect_client()

    #
    # Helpers
    #

    def save_window_size(self):
        window_size = [self.get_size().width, self.get_size().height]
        fernschreiber.config_manager.set_window_size(window_size)

