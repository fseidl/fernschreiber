from gi.repository import Gtk
from fernschreiber.room_sidebar import RoomSidebar


class Overview():
    builder = NotImplemented

    window = NotImplemented
    client_manager = NotImplemented
    room_sidebar = NotImplemented

    headerbar = NotImplemented

    def __init__(self, window, client_manager):
        self.window = window
        self.client_manager = client_manager

        self.builder = Gtk.Builder()
        self.builder.add_from_resource(
            "/run/terminal/Fernschreiber/overview.ui"
        )

        self.client_manager.run_on_thread(self.client_manager.log_in)

        self.set_overview_headerbar()
        self.assemble_overview()

        # Build Sidebar
        self.room_sidebar = RoomSidebar(self)

    def set_overview_headerbar(self):
        self.headerbar = self.builder.get_object("headerbar")
        self.window.set_titlebar(self.headerbar)

    def assemble_overview(self):
        self.window.add(self.builder.get_object("overview_page"))

