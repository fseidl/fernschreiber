from gi.repository import Gtk
from fernschreiber.room_sidebar_row import RoomSidebarRow


class RoomSidebar():
    window = NotImplemented
    client_manager = NotImplemented

    room_list_box = NotImplemented

    def __init__(self, instance):
        self.window = instance.window
        self.client_manager = instance.client_manager
        self.room_list_box = instance.builder.get_object("room_list_box")

        self.fill_dialogs_sidebar()

    def fill_dialogs_sidebar(self):
        for dialog in self.client_manager.run_on_thread(self.client_manager.client.get_dialogs):
            self.room_list_box.add(RoomSidebarRow(self.client_manager, dialog))

