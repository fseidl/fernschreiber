from gi.repository import Gtk
from gi.repository.GdkPixbuf import Pixbuf
import fernschreiber.config_manager
import threading


class RoomSidebarRow(Gtk.ListBoxRow):
    builder = NotImplemented
    client_manager = NotImplemented
    loop = NotImplemented

    photo = NotImplemented

    dialog = NotImplemented

    def __init__(self, client_manager, dialog):
        Gtk.ListBoxRow.__init__(self)
        self.set_name("RoomSidebarRow")

        self.client_manager = client_manager
        self.dialog = dialog

        self.assemble_room_sidebar_row()

    def assemble_room_sidebar_row(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_resource(
            "/run/terminal/Fernschreiber/room_row.ui"
        )

        self.set_row_room_photo()
        self.set_row_name_label()
        self.set_row_preview_label()

        self.show_all()
        self.add(self.builder.get_object("room_event_box"))

    def set_row_room_photo(self):
        threading.Thread(target=self.start_thread).start()

    def start_thread(self):
        path = fernschreiber.config_manager.photos_dir + str(self.dialog.entity.photo.photo_small.secret) + ".jpeg"
        bytes = self.client_manager.run_on_thread(self.client_manager.client.download_file, self.dialog.entity.photo.photo_small)
        self.client_manager.join()

        file = open(path, "wb")
        file.write(bytes)
        file.close()

        pix = Pixbuf.new_from_file_at_size(path, 36, 36)
        self.builder.get_object("room_photo").set_from_pixbuf(pix)

    def set_row_name_label(self):
        self.builder.get_object("room_name_label").set_text(self.dialog.title)

    def set_row_preview_label(self):
        self.builder.get_object("room_preview_label").set_text(str(self.dialog.message.message))

